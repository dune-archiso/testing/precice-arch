
- [ ] Test with other solvers like UMFPACK or MUMPS.
- [ ] Test with several versions of deal.II, nutils.
<!-- - [] Test SU2-adapter with the latest version. -->
- [ ] Apply patches for the test code.
- [ ] Try to reuse bash functions in one single script.
- [ ] Look if we are running deal.II on serial or parallel
- [ ] Run all [deal.II examples](https://www.dealii.org/developer/doxygen/deal.II/Tutorial.html).

[](https://precice.org/adapter-dealii-get.html)

Look if this option is enabled!

```console
-D DEAL_II_COMPONENT_EXAMPLES="OFF" \
```

```console
cmake \
    -D CMAKE_BUILD_TYPE="DebugRelease" \
    -D CMAKE_CXX_FLAGS="-march=native \
    -D DEAL_II_CXX_FLAGS_RELEASE="-O3" \
    -D DEAL_II_WITH_UMFPACK="ON" \
    -D DEAL_II_WITH_THREADS="ON" \
    -D DEAL_II_COMPONENT_EXAMPLES="OFF" \
    -D CMAKE_INSTALL_PREFIX=/path/install/dir \
    ../dealii

make -j 4
```

Try with `umfpack` or `mumps`.