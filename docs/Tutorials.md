## Tutorials cases ✅ [`v202211.0`](https://github.com/precice/tutorials/releases/tag/v202211.0) with preCICE `v2.5.0` executed with 4 vCPUs, 16 GB RAM [(n2-standard-4)](https://cloud.google.com/compute/docs/general-purpose-machines#n2-standard)


- [`flow-over-heated-plate`](https://precice.org/tutorials-flow-over-heated-plate.html) ⭐

| Fluid $`S_1`$                                                                           | Solid $`S_2`$                                                                           |                                                  Is it coupling successfully? (yes, it includes artifact)                                                   |
| :-------------------------------------------------------------------------------------- | :-------------------------------------------------------------------------------------- | :---------------------------------------------------------------------------------------------------------------------------------------------------------: |
| [`openfoam-com-precice 1.2.1`](https://aur.archlinux.org/packages/openfoam-com-precice) | [`openfoam-com-precice 1.2.1`](https://aur.archlinux.org/packages/openfoam-com-precice) | [I think so](https://gitlab.com/dune-archiso/testing/precice-arch/-/jobs/artifacts/main/download?job=test_tutorial_fohp_openfoam-com-openfoam-com_precice)  |
| [`openfoam-com-precice 1.2.1`](https://aur.archlinux.org/packages/openfoam-com-precice) | [`python-fenicsprecice 1.4.0`](https://aur.archlinux.org/packages/python-fenicsprecice) | [I think so](https://gitlab.com/dune-archiso/testing/precice-arch/-/jobs/artifacts/main/download?job=test_tutorial_fohp_openfoam-com-fenicsprecice_precice) |
| [`openfoam-com-precice 1.2.1`](https://aur.archlinux.org/packages/openfoam-com-precice) | [`nutils 7.2`](https://aur.archlinux.org/packages/python-nutils)                        |    [I think so](https://gitlab.com/dune-archiso/testing/precice-arch/-/jobs/artifacts/main/download?job=test_tutorial_fohp_openfoam-com-nutils_precice)     |
| [`openfoam-com-precice 1.2.1`](https://aur.archlinux.org/packages/openfoam-com-precice) | [`dune-fem 2.9.0`](https://aur.archlinux.org/packages/python-dune-fem)                  |   [I think so](https://gitlab.com/dune-archiso/testing/precice-arch/-/jobs/artifacts/main/download?job=test_tutorial_fohp_openfoam-com-dune-fem_precice)    |

⭐ [Working nutils v7.2 with the patch _Minor update to first two Nutils examples #273_](https://github.com/precice/tutorials/pull/273)

- [`partitioned-heat-conduction`](https://precice.org/tutorials-partitioned-heat-conduction.html) ⭐

| Dirichlet-kind $`S_1`$                                                                  | Neumann-kind $`S_2`$                                                                    |                                                  Is it coupling successfully? (yes, it includes artifact)                                                   |
| :-------------------------------------------------------------------------------------- | :-------------------------------------------------------------------------------------- | :---------------------------------------------------------------------------------------------------------------------------------------------------------: |
| [`python-fenicsprecice 1.4.0`](https://aur.archlinux.org/packages/python-fenicsprecice) | [`python-fenicsprecice 1.4.0`](https://aur.archlinux.org/packages/python-fenicsprecice) | [I think so](https://gitlab.com/dune-archiso/testing/precice-arch/-/jobs/artifacts/main/download?job=test_tutorial_phc_fenicsprecice-fenicsprecice_precice) |
| [`python-fenicsprecice 1.4.0`](https://aur.archlinux.org/packages/python-fenicsprecice) | [`nutils 7.2`](https://aur.archlinux.org/packages/python-nutils)                        |    [I think so](https://gitlab.com/dune-archiso/testing/precice-arch/-/jobs/artifacts/main/download?job=test_tutorial_phc_fenicsprecice-nutils_precice)     |
| [`python-fenicsprecice 1.4.0`](https://aur.archlinux.org/packages/python-fenicsprecice) | [`openfoam-com-precice 1.2.1`](https://aur.archlinux.org/packages/openfoam-com-precice) | [I think so](https://gitlab.com/dune-archiso/testing/precice-arch/-/jobs/artifacts/main/download?job=test_tutorial_phc_fenicsprecice-openfoam-com_precice)  |
| [`nutils 7.2`](https://aur.archlinux.org/packages/python-nutils)                        | [`python-fenicsprecice 1.4.0`](https://aur.archlinux.org/packages/python-fenicsprecice) |    [I think so](https://gitlab.com/dune-archiso/testing/precice-arch/-/jobs/artifacts/main/download?job=test_tutorial_phc_nutils-fenicsprecice_precice)     |
| [`nutils 7.2`](https://aur.archlinux.org/packages/python-nutils)                        | [`nutils 7.2`](https://aur.archlinux.org/packages/python-nutils)                        |        [I think so](https://gitlab.com/dune-archiso/testing/precice-arch/-/jobs/artifacts/main/download?job=test_tutorial_phc_nutils-nutils_precice)        |
| [`nutils 7.2`](https://aur.archlinux.org/packages/python-nutils)                        | [`openfoam-com-precice 1.2.1`](https://aur.archlinux.org/packages/openfoam-com-precice) |     [I think so](https://gitlab.com/dune-archiso/testing/precice-arch/-/jobs/artifacts/main/download?job=test_tutorial_phc_nutils-openfoam-com_precice)     |
| [`openfoam-com-precice 1.2.1`](https://aur.archlinux.org/packages/openfoam-com-precice) | [`python-fenicsprecice 1.4.0`](https://aur.archlinux.org/packages/python-fenicsprecice) | [I think so](https://gitlab.com/dune-archiso/testing/precice-arch/-/jobs/artifacts/main/download?job=test_tutorial_phc_openfoam-com-fenicsprecice_precice)  |
| [`openfoam-com-precice 1.2.1`](https://aur.archlinux.org/packages/openfoam-com-precice) | [`nutils 7.2`](https://aur.archlinux.org/packages/python-nutils)                        |     [I think so](https://gitlab.com/dune-archiso/testing/precice-arch/-/jobs/artifacts/main/download?job=test_tutorial_phc_openfoam-com-nutils_precice)     |
| [`openfoam-com-precice 1.2.1`](https://aur.archlinux.org/packages/openfoam-com-precice) | [`openfoam-com-precice 1.2.1`](https://aur.archlinux.org/packages/openfoam-com-precice) |  [I think so](https://gitlab.com/dune-archiso/testing/precice-arch/-/jobs/artifacts/main/download?job=test_tutorial_phc_openfoam-com-openfoam-com_precice)  |

⭐ [Working nutils v7.2 with the patch _Minor update to first two Nutils examples #273_](https://github.com/precice/tutorials/pull/273)

- [`turek-hron-fsi3`](https://precice.org/tutorials-turek-hron-fsi3.html)

| Fluid $`S_1`$                                                                           | Solid $`S_2`$                           |                                               Is it coupling successfully? (yes, it includes artifact)                                               |
| :-------------------------------------------------------------------------------------- | :-------------------------------------- | :--------------------------------------------------------------------------------------------------------------------------------------------------: |
| [`openfoam-com-precice 1.2.1`](https://aur.archlinux.org/packages/openfoam-com-precice) | [*`deal-ii-precice-git r240.005933d`]() | [I think so](https://gitlab.com/dune-archiso/testing/precice-arch/-/jobs/artifacts/main/download?job=test_tutorial_thf_openfoam-com-deal-ii_precice) |

- [`multiple-perpendicular-flaps`](https://precice.org/tutorials-multiple-perpendicular-flaps.html)

| Fluid $`S_1`$                                                                                      | Solid left - right $`S_2`$, $`S_3`$     |                                                   Is it coupling successfully? (yes, it includes artifact)                                                    |
| :------------------------------------------------------------------------------------------------- | :-------------------------------------- | :-----------------------------------------------------------------------------------------------------------------------------------------------------------: |
| [`openfoam-com-precice 1.2.1 (serial)`](https://aur.archlinux.org/packages/openfoam-com-precice)   | [*`deal-ii-precice-git r240.005933d`]() |  [I think so](https://gitlab.com/dune-archiso/testing/precice-arch/-/jobs/artifacts/main/download?job=test_tutorial_mpf_openfoam-com_serial-deal-ii_precice)  |
| [`openfoam-com-precice 1.2.1 (parallel)`](https://aur.archlinux.org/packages/openfoam-com-precice) | [*`deal-ii-precice-git r240.005933d`]() | [I think so](https://gitlab.com/dune-archiso/testing/precice-arch/-/jobs/artifacts/main/download?job=test_tutorial_mpf_openfoam-com_parallel-deal-ii_precice) |

- [`elastic-tube-3d`](https://precice.org/tutorials-elastic-tube-3d.html)

| Fluid $`S_1`$                                                                           | Solid $`S_2`$                                                                           |                                                  Is it coupling successfully? (yes, it includes artifact)                                                  |
| :-------------------------------------------------------------------------------------- | :-------------------------------------------------------------------------------------- | :--------------------------------------------------------------------------------------------------------------------------------------------------------: |
| [`openfoam-com-precice 1.2.1`](https://aur.archlinux.org/packages/openfoam-com-precice) | [`calculix-precice 2.20.0`](https://aur.archlinux.org/packages/calculix-precice)        |   [I think so](https://gitlab.com/dune-archiso/testing/precice-arch/-/jobs/artifacts/main/download?job=test_tutorial_et3_openfoam-com-calculix_precice)    |
| [`openfoam-com-precice 1.2.1`](https://aur.archlinux.org/packages/openfoam-com-precice) | [`python-fenicsprecice 1.4.0`](https://aur.archlinux.org/packages/python-fenicsprecice) | [I think so](https://gitlab.com/dune-archiso/testing/precice-arch/-/jobs/artifacts/main/download?job=test_tutorial_et3_openfoam-com-fenicsprecice_precice) |

- [`elastic-tube-1d`](https://precice.org/tutorials-elastic-tube-1d.html)

| Fluid $`S_1`$                                                                     | Solid $`S_2`$                                                                     |                                              Is it coupling successfully? (yes, it includes artifact)                                               |
| :-------------------------------------------------------------------------------- | :-------------------------------------------------------------------------------- | :-------------------------------------------------------------------------------------------------------------------------------------------------: |
| [`precice 2.5.0`](https://aur.archlinux.org/packages/precice)                     | [`precice 2.5.0`](https://aur.archlinux.org/packages/precice)                     |   [I think so](https://gitlab.com/dune-archiso/testing/precice-arch/-/jobs/artifacts/main/download?job=test_tutorial_et1_precice-precice_precice)   |
| [`precice 2.5.0`](https://aur.archlinux.org/packages/precice)                     | [`python-pyprecice 2.5.0.1`](https://aur.archlinux.org/packages/python-pyprecice) |  [I think so](https://gitlab.com/dune-archiso/testing/precice-arch/-/jobs/artifacts/main/download?job=test_tutorial_et1_precice-pyprecice_precice)  |
| [`python-pyprecice 2.5.0.1`](https://aur.archlinux.org/packages/python-pyprecice) | [`precice 2.5.0`](https://aur.archlinux.org/packages/precice)                     |  [I think so](https://gitlab.com/dune-archiso/testing/precice-arch/-/jobs/artifacts/main/download?job=test_tutorial_et1_pyprecice-precice_precice)  |
| [`python-pyprecice 2.5.0.1`](https://aur.archlinux.org/packages/python-pyprecice) | [`python-pyprecice 2.5.0.1`](https://aur.archlinux.org/packages/python-pyprecice) | [I think so](https://gitlab.com/dune-archiso/testing/precice-arch/-/jobs/artifacts/main/download?job=test_tutorial_et1_pyprecice-pyprecice_precice) |

- [`flow-over-heated-plate-nearest-projection`](https://precice.org/tutorials-flow-over-heated-plate-nearest-projection.html)

| Fluid $`S_1`$                                                                           | Solid $`S_2`$                                                                           |                                                   Is it coupling successfully? (yes, it includes artifact)                                                   |
| :-------------------------------------------------------------------------------------- | :-------------------------------------------------------------------------------------- | :----------------------------------------------------------------------------------------------------------------------------------------------------------: |
| [`openfoam-com-precice 1.2.1`](https://aur.archlinux.org/packages/openfoam-com-precice) | [`openfoam-com-precice 1.2.1`](https://aur.archlinux.org/packages/openfoam-com-precice) | [I think so](https://gitlab.com/dune-archiso/testing/precice-arch/-/jobs/artifacts/main/download?job=test_tutorial_fohpnp_openfoam-com-openfoam-com_precice) |

- [`flow-over-heated-plate-steady-state`](https://precice.org/tutorials-flow-over-heated-plate-steady-state.html)

| Fluid $`S_1`$                                                                           | Solid $`S_2`$          |                                                   Is it coupling successfully? (yes, it includes artifact)                                                   |
| :-------------------------------------------------------------------------------------- | :--------------------- | :----------------------------------------------------------------------------------------------------------------------------------------------------------: |
| [`openfoam-com-precice 1.2.1`](https://aur.archlinux.org/packages/openfoam-com-precice) | [`code_aster 1.1.0`]() | [I think so](https://gitlab.com/dune-archiso/testing/precice-arch/-/jobs/artifacts/main/download?job=test_tutorial_fohpnp_openfoam-com-openfoam-com_precice) |

- [`heat-exchanger`](https://precice.org/tutorials-heat-exchanger.html)

| Fluid $`S_1`$                                                                           | Solid $`S_2`$                                                                    |                                                   Is it coupling successfully? (yes, it includes artifact)                                                   |
| :-------------------------------------------------------------------------------------- | :------------------------------------------------------------------------------- | :----------------------------------------------------------------------------------------------------------------------------------------------------------: |
| [`openfoam-com-precice 1.2.1`](https://aur.archlinux.org/packages/openfoam-com-precice) | [`calculix-precice 2.20.0`](https://aur.archlinux.org/packages/calculix-precice) | [I think so](https://gitlab.com/dune-archiso/testing/precice-arch/-/jobs/artifacts/main/download?job=test_tutorial_fohpnp_openfoam-com-openfoam-com_precice) |

*It is not yet in the [Arch User Repository](https://aur.archlinux.org/packages?O=0&K=precice), but it compiles fine.

**It takes more time (> 15 minutes) to (not) finish than the previous cases.