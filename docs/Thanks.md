## Many thanks 🌟

To everyone who contribute with the code used here.
In special to [tutorial's contributors](https://github.com/precice/tutorials/graphs/contributors),
[[arch4edu]](https://wiki.archlinux.org/title/Unofficial_user_repositories_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87)#arch4edu)
maintainer for keeping many of the packages used up to date and functional, for instance OpenFOAM and
[GitLab for Open Source Program](https://about.gitlab.com/solutions/open-source/join) for grant a license for 1 year.
<!-- https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html -->
