Go to https://gitpod.io/#https://gitlab.com/dune-archiso/testing/precice-arch

```console
$ pacman -Qs precice
local/calculix-precice 2.20.0-1
    preCICE-adapter for the CSM code CalculiX
local/openfoam-com-precice 1.2.2-1
    preCICE adapter for OpenFOAM
local/precice 2.5.0-1
    A Coupling Library for Partitioned Multi-Physics Simulations on Massively Parallel Systems
local/precice-config-visualizer-git 20230224-1
    A tool for visualizing a preCICE configuration file as a dot file
local/python-fenicsprecice 1.4.0-1
    FEniCS-preCICE adapter is a preCICE adapter for the open source computing platform FEniCS
local/python-micro-manager-precice 0.2.1-1
    micro-manager-precice is a package which facilitates two-scale macro-micro coupled simulations using preCICE
local/python-pyprecice 2.5.0.2-1
    Python language bindings for the preCICE coupling library
```

```console
$ pacman -Ql openfoam-com | less
$ source /opt/OpenFOAM/OpenFOAM-v2206/etc/bashrc
perpendicular-flap/fluid-openfoam/
precice-Fluid-events-summary.log
precice-Fluid-events.json
precice-Fluid-iterations.log
precice-Solid-watchpoint-Flap-Tip.log

perpendicular-flap/solid-calculix/
precice-Solid-convergence.log
precice-Solid-events-summary.log
precice-Solid-events.json
precice-Solid-iterations.log
precice-Solid-watchpoint-Flap-Tip.log
```