## [Licenses](https://spdx.org/licenses)

| License                                 | Package                                                                   |
| :-------------------------------------- | :------------------------------------------------------------------------ |
| GPL3                                    | [OpenFOAM - ESI Group](https://www.openfoam.com/documentation/licencing)  |
| LGPL3                                   | [preCICE](https://github.com/precice/precice/blob/develop/LICENSE)        |
| APL2                                    | [cython](https://github.com/cython/cython/blob/master/LICENSE.txt)        |
| BSD-2-Clause                            | [mpi4py](https://github.com/mpi4py/mpi4py/blob/master/LICENSE.rst)        |
| BSD-2-Clause                            | [PETSc](https://petsc.org/release/install/license)                        |
| Boost Software License                  | [Boost](https://www.boost.org/users/license.html)                         |
| MIT                                     | [libxml2](https://gitlab.gnome.org/GNOME/libxml2/-/blob/master/Copyright) |
| Liberal BSD license                     | [NumPy](https://numpy.org/doc/stable/license.html)                        |
| BSD-3-Clause                            | [Cmake](https://cmake.org/licensing)                                      |
| Apache 2.0 License with LLVM exceptions | [Clang](https://clang.llvm.org)                                           |
| GPL2 with runtime exception             | [DUNE](https://dune-project.org/about/license)                            |
| GPL3                                    | [DuMuˣ](https://dune-project.org/about/license)                           |
| GPL3                                    | [code_aster](https://www.code-aster.org/spip.php?article306)              |
| GPL2                                    | [Calculix](http://www.calculix.de)                                        |
| LGPL2                                   | [deal.II](https://github.com/dealii/dealii/blob/master/LICENSE.md)        |
| LGPL3                                   | [dolfinx](https://github.com/FEniCS/dolfinx/blob/main/COPYING)            |
| MIT                                     | [nutils](https://github.com/evalf/nutils/blob/master/LICENSE)             |
| LGPL2                                   | [SU2](https://github.com/su2code/SU2/blob/master/LICENSE.md)              |
| LGPL2                                   | [Elmer](http://www.elmerfem.org/blog/license)                             |

![](https://i.stack.imgur.com/GvOBw.png)

![](https://ars.els-cdn.com/content/image/1-s2.0-S2211467X17300949-gr3_lrg.jpg)