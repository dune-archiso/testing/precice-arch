## [Usage 🚇](https://gitlab.com/dune-archiso/testing/precice-arch/-/pipelines/latest)

### Add repository

1. This repository needs have the `[arch4edu]` repository. See [these steps](https://github.com/arch4edu/arch4edu/wiki/Add-arch4edu-to-your-Archlinux).

2. Import GPG key from GPG servers.

```console
[user@hostname ~]$ sudo pacman-key --init
[user@hostname ~]$ sudo pacman-key --recv-keys 2403871B121BD8BB
[user@hostname ~]$ sudo pacman-key --finger 2403871B121BD8BB
[user@hostname ~]$ sudo pacman-key --lsign-key 2403871B121BD8BB
```

2. Append the following lines to `/etc/pacman.conf`:

```toml
[precice-arch]
SigLevel = Required DatabaseOptional
Server = https://dune-archiso.gitlab.io/testing/precice-arch/$arch
```

### List packages

To show actual packages list:

```console
[user@hostname ~]$ pacman -Sl precice-arch
```

### Install packages

To install package:

```console
[user@hostname ~]$ sudo pacman --needed --noconfirm -Syu
[user@hostname ~]$ sudo pacman -S openfoam-com-precice dune-precice-git
```