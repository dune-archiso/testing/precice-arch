[Running every friday at 4:00 AM UTC-5](https://gitlab.com/dune-archiso/testing/precice-arch/-/pipeline_schedules).

# [`precice-arch`](https://gitlab.com/dune-archiso/testing/precice-arch) packages for Arch Linux

[![pipeline status](https://gitlab.com/dune-archiso/testing/precice-arch/badges/main/pipeline.svg)](https://gitlab.com/dune-archiso/testing/precice-arch/-/commits/main)
[![coverage report](https://gitlab.com/dune-archiso/testing/precice-arch/badges/main/coverage.svg)](https://gitlab.com/dune-archiso/testing/precice-arch/-/commits/main)

This is a third-party auto-updated repository with the following [dependencies](https://gitlab.com/dune-archiso/testing/precice-arch/-/raw/main/dependencies.x86_64) and [tarballs](https://gitlab.com/dune-archiso/testing/precice-arch/-/raw/main/adapters.x86_64).
Mostly packages required for [preCICE adapters](https://open-research-europe.ec.europa.eu/articles/2-51/v1/pdf).

## [Usage 🚇](docs/Usage.md)

## [Tutorials cases ✅ `v202403.0` with preCICE `v3.0.0` executed with 4 vCPUs, 16 GB RAM (n2-standard-4)](docs/Tutorials.md)

## [Pipeline assets](docs/Assets.md)

## [Non-comprehensive Awesome Arch User Repository](docs/Awesome.md)

## [Licenses](docs/Licenses.md)

## [Many thanks 🌟](docs/Thanks.md)
