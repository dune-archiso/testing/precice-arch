==================
Release History 📖
==================

.. v0.2.0
.. ------

.. :Date: Apr 5, 2024

.. * Initial release.

v0.1.0
------

:Date: Apr 5, 2022

* Initial release.

v0.1.0-alpha
------------

:Date: May 5, 2022

* Tutorial phc working nutils 7.0, |patch|.
* Start keeping changelog.

.. |patch| raw:: html

   <tt><a href="https://github.com/precice/tutorials/pull/273">patch</a></tt>

v0.1.0-beta

:Date: Feb 22, 2022

* Rename to precice-arch.

v0.1.0-gamma

:Date: Dec 15, 2021

* Create dune-precice repository.
